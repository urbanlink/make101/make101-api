import { 
  Column, 
  CreateDateColumn,
  Entity, 
  OneToMany, 
  PrimaryGeneratedColumn
} from 'typeorm';
import ToMake from '../tomake/tomake.entity';

@Entity()
class User {

  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column({
    unique: true,
  })
  auth0: string;

  @Column({
    unique: true,
    select: false
  })
  email: string;

  @Column({
    select: false
  })
  email_verified: boolean;

  @Column({ unique: true })
  nickname: string;
  
  @Column({ unique: true })
  slug: string;

  @Column()
  picture: string

  @Column()
  account_plan: string
  
  @Column({
    type: 'timestamptz', 
    nullable: true 
  })
  account_plan_end: Date

  @Column({ default: false })
  admin: boolean
  
  @Column({ type: 'text', nullable: true })
  bio: string

  @Column({ nullable: true })
  firstname: string

  @Column({ nullable: true })
  lastname: string

  @Column({
    type: 'timestamptz',
    nullable: true
  })
  signed_up: Date
  
  @Column({ default: 'daily' })
  email_digest_schedule: string

  @Column({ 
    type: 'timestamptz',
    nullable: true 
  })
  last_email_digest: Date

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
  createdAt: Date

  @OneToMany(() => ToMake, tomake => tomake.user)
  tomakes: ToMake[];

}

export default User;
