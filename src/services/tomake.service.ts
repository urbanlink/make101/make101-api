import { 
  getRepository,
  IsNull 
} from 'typeorm'
import urlSlug from 'url-slug'
import ToMake from '../entities/tomake/tomake.entity'
import logger from '../utils/logger'

// helper to create an synchronous loop
const asyncForEach = async (array:any[], callback:any) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const CreateItemSlug = (makeItem: ToMake) => {
  return urlSlug(makeItem.title).substr(0,100) + '-' + makeItem.id
}

// Fix slug for existing items. 
const FixItems = async () => {
  let makeItems: ToMake[];
  try {
    makeItems = await getRepository(ToMake).find({
      where: { slug: IsNull()}
    })
  } catch(err) {
    console.warn(err);
  }
  logger.info(`Found ${makeItems.length} items without a slug`)
  await asyncForEach(makeItems, async (item: ToMake) => {
    item.slug = CreateItemSlug(item)
    getRepository(ToMake).save(item)
    console.log(item.slug);
  })
  logger.info(`Done updating slugs for items`)
}

/*
 *
 *
 * 
 */
const showToMakeItem = async (slug: string) => {

  logger.debug(`[ tomake.service ] showToMakeItem: Show item slug ${slug}`);
  let item: ToMake; 
  
  try {
    item = await getRepository(ToMake).createQueryBuilder('to_make')
      .leftJoinAndSelect('to_make.user', 'user')
      // .where('user.auth0 = :authId', { authId: sub })
      .where('to_make.slug = :slug', { slug: slug })
      .getOne();
  } catch(err) {
    return Promise.reject(err); 
  }

  return item;
}

export {
  CreateItemSlug, 
  showToMakeItem,
  FixItems
}