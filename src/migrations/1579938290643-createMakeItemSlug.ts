import {
    getRepository,
    MigrationInterface, 
    QueryRunner,
    IsNull
} from "typeorm";
import ToMake from '../entities/tomake/tomake.entity'

export class createMakeItemSlug1579938290643 implements MigrationInterface {
    
    public async up(queryRunner: QueryRunner): Promise<any> {
        console.log('createMakeItemSlug1579938290643 up');
        const items = await getRepository(ToMake)
            .find({
                where: {
                    slug: IsNull()
                }
            })
        console.log('Items found: ', items.length)
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        console.log('createMakeItemSlug1579938290643 down');
    }

}
