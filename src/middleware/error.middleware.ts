import { NextFunction, Request, Response } from 'express';
import HttpException from '../exceptions/HttpException';
import logger from '../utils/logger';

function errorMiddleware(error: HttpException, req: Request, res: Response, _next: NextFunction) {

  if (error.name === 'UnauthorizedError') {
    console.log(`Unauthorized error for request: ${req.originalUrl}`)
  }
  
  const status: number = error.status || 500;
  const message: string = error.message || 'Something went wrong';
  logger.warn(JSON.stringify(error));
  console.warn(status, error.name, message)

  res.status(status).json({ message, status });
}

export default errorMiddleware;
