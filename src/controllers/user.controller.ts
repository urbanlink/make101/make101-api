import * as express from 'express';
import { getRepository, Like, Not } from 'typeorm';
import Controller from '../interfaces/controller.interface';
import User from '../entities/user/user.entity';
import logger from '../utils/logger';
import { GetUser } from '../services/user.service'; 
import { DeleteAuth0User } from '../services/auth0.service'; 
import HttpException from '../exceptions/HttpException';

class UserController implements Controller {

  public path = '/user';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/me`, this.getCurrentUser);
    this.router.get(`${this.path}/search`, this.searchUser);
    this.router.delete(`${this.path}`, this.deleteUser);
  }

  /**
   *
   *
   * 
   **/
  private getCurrentUser = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    let user: User;
    const sub: string = req.user.sub;

    // Get user from database 
    try {
      user = await GetUser(sub);
    } catch (err) {
      return next(new HttpException(400, err))
    }

    return res.json(user); 
  }

  /**
   * 
   * 
   * 
   */
  private searchUser = async (req: express.Request, res: express.Response, next: express.NextFunction) => {

    logger.info('[ account.controller search ] - Query for: ', req.query);

    // Validation
    if (!req.query.nickname) { return next(new HttpException(403, 'No nickname provided')); }
    let nickname = req.query.nickname.toString();
    if (nickname.length < 3) { return next(new HttpException(403, 'Nickname too short')); }
    if (nickname.length > 25) { return next(new HttpException(403, 'Nickname too long')); }

    //
    const users: User[] = await getRepository(User).find({
      where: {
        nickname: Like(`%${nickname}%`),
        auth0: Not(req.user.sub)
      },
      select: ['nickname', 'picture', 'auth0'],
      take:20
    });

    return res.json(users);
  }

  /**
   * 
   * 
   * 
   */
  private deleteUser = async (req: express.Request, res: express.Response, _next: express.NextFunction) => {
    try {
      // Delete user from local database
      const user = await getRepository(User).findOne({
        where: { auth0: req.user.sub }
      });
      if (!user) { return false; }
      const r = await getRepository(User).remove(user);
      console.log(r);

      // Delete user from auth0
      const a: any = await DeleteAuth0User(req.user.sub);
      console.log('delete user result', a);

      // Handle result
      return res.json(true);

    } catch(err) {
      logger.warn(err);
      return res.status(500).json(err);
    }
  }
}

export default UserController;
