// Fetch make items for a given user-sub 
// url request: //api.make101.urbanlink.nl/widgets/listing/widget-listing.html?key=${m101_key}

// Wait for loading to be complete 
window.onload = function(e) {
  var param; 
  try {
    // Get search params from the url
    param = window.location.search.substring(1).split('&')[ 0].split('=');
  } catch (err) {
    this.console.warn(err); 
    return; 
  }
  this.console.log(param); 
  
  if (param[ 0] !== 'key') { 
    console.warn('no key provided'); 
    document.getElementById('empty').innerHTML='Error: No key provided.'; 
    return; 
  }

  // Define the key 
  var key = param[ 1];
  if (!key) { return; }
  // var url = window.location.origin + '/make?key=' + key;
  var url = 'https://api.make101.urbanlink.nl/make?key='+key;

  var xhr = new XMLHttpRequest()
      xhr.open('GET', url, true)
      xhr.withCredentials = true
      xhr.setRequestHeader('Content-Type', 'application/json')
      xhr.send()

  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
      try {
        var r = JSON.parse(xhr.response);
        handleResult(r);
      } catch(err) {
        console.warn(err);
        document.getElementById('empty').innerHTML='Error: There was an error parsing the items.'; 
      }
    }
  }

  function handleResult(items) {
    
    if (!items || (items.length === 0)) {
      console.log('no items')
      document.getElementById('empty').innerHTML='There are no items created yet.'; 
    } else {

      var ul = document.getElementById('list');
      for (var i = 0; i < items.length; i++) {
        var li = document.createElement('li');
        li.appendChild(document.createTextNode(items[i].title));
        ul.appendChild(li);
      }
    }
  }
}
