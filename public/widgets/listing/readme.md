* https://blog.jenyay.com/building-javascript-widget/
* https://github.com/jenyayel/js-widget/blob/master/demo/index.html 


## Share

<script>
  (function (w,d,s,o,f,js,fjs) {
      w['M101ListObject']=o;w[o] = w[o] || function () { (w[o].q = w[o].q || []).push(arguments) };
      js = d.createElement(s), fjs = d.getElementsByTagName(s)[0];
      js.id = o; js.src = f; js.async = 1; fjs.parentNode.insertBefore(js, fjs);
  }(window, document, 'script', 'm101', 'https://make101.urbanlink.nl/widgets/list.js'));
  mw('init', { 
    key: '123', 
    width: 220,
    height: 400, 
    sort: 'random',
    filter: 'none'
  });
</script>