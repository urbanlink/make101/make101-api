
window.onload = function(e) {
  console.log('loaded');
  var widget_link, iframe, i, widget_links;
  widget_links = document.getElementsByClassName('make101_listing_widget');

  for (i = 0; i < widget_links.length; i++) {

    widget_link = widget_links[ i];
    console.log(widget_link);

    var apiUrl = widget_link.getAttribute("data-url");
    var authId = widget_link.getAttribute("data-authid");

    // Create iFrame
    iframe = document.createElement('iframe');
    iframe.setAttribute('src', apiUrl + '/widgets/listing/listing.widget.html#authId=' + authId + '&apiUrl=' + apiUrl);
    iframe.setAttribute('width', '300');
    iframe.setAttribute('height', '450');
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('scrolling', 'yes');
    iframe.setAttribute('id', 'listing-frame')
    iframe.setAttribute('authId', authId);
    iframe.setAttribute('apiUrl', apiUrl);

    // Add to screen
    widget_link.parentNode.replaceChild(iframe, widget_link);

  }
}
